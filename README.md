## FaceSynthetics

Face Synthetics dataset is a collection of diverse synthetic face images with ground truth labels. It was introduced in the paper [Fake It Till You Make It: Face analysis in the wild using synthetic data alone](https://microsoft.github.io/FaceSynthetics/).

This repository provides an example of dataset description, including a data model, for the Face Synthetics dataset. 
- The PPTX (and CSV) files can be found under [docs/](docs).
- The JSON data model can be found in the file [facesynthetics.datamodel.json](facesynthetics.datamodel.json).

### Data Model

The data model is intended to help document datasets before using them with Bonseyes Datatools. The Face Synthetics data model is created from a single JSON file that contains a serialization example of the dataset with a single example entry and complete information of annotation categories. This JSON covers the following:

- images (id, filename, width, height, channels)
- annotations (id, image_id, bbox, keypoints, segmentation_per_pixel)
- keypoint categories (id, name)
- segmentation categories (id, name, pixel_value, color_map)

### Test Script [Optional]

You can optionally test your JSON data model before creating a datatool. To test the data model, a test script is provided in the [tests/](tests). To run the test script, follow the instructions contained in the file [docs/README.md](docs/README.md).

**Note:** this is a preliminary test. For complete data model testing, use the datatool testing script provided by the [datatool generator](https://gitlab.com/bonseyes/artifacts/data_tools/datatool-template/-/tree/master/data_model_tests).

### Licensing and Contributing

This project is licensed under the terms and conditions of the MIT License. A copy of the license is accessible here: [LICENSE](LICENSE).

Details on contributing to this project are found in the file [docs/CONTRIBUTING.md](docs/CONTRIBUTING.md).

### Disclaimer

Bonseyes Community Association provides only scripts for downloading and preprocessing public datasets. We do not own these datasets and are not responsible for their quality or maintenance.

Please ensure that you have the permission to use a dataset under the dataset's license. We are not a law firm and does not provide legal advice. Our open source tools are meant to be a jumping-off point, not a final word on whether your use is compatible with a particular license. For example, some licenses may allow commercial use, but still have requirements about attribution and other things. It is your responsibility alone to read the license and follow it.

Bonseyes Community Association provides this information and tools on an “as-is” basis without warranties of any kind, and disclaims liability for all damages resulting from your use of the license information and tools. Please seek the advice of a licensed legal professional in your jurisdiction if you have any questions about a particular license.

To dataset owners: If you do not want your dataset to be included on Bonseyes, or wish to update it in any way, we will remove or update all public content upon your request. You can contact us with Gitlab issues. Your understanding and contribution to this community are greatly appreciated.
